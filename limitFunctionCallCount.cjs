function limitFunctionCallCount(cb, n) {
    let cCounter = 0; 
    return function(...args) {
        if (cCounter < n) {
            cCounter++;
            return cb(...args);
        }
        else{
            return null;
        }
    };
}

module.exports = limitFunctionCallCount;

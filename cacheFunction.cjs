// cacheFunction.cjs

function cacheFunction(cb) {
    const cache = {}; // Object to store cached results

    return function(...args) {
        const key = JSON.stringify(args); // Generate a unique key for the arguments
        if (!(key in cache)) {
            cache[key] = cb(...args); // Invoke cb and cache the result
        }
        return cache[key]; // Return cached result
    };
}

module.exports = cacheFunction;

// testCacheFunction.cjs

const cacheFunction = require('../cacheFunction.cjs');

// Sample callback function to be cached
function add(a, b) {
    console.log('Function called');
    return a + b;
}

// Create a cached function
const cachedAdd = cacheFunction(add);

// Call the cached function multiple times with the same arguments
console.log(cachedAdd(3, 5)); 
console.log(cachedAdd(3, 5)); 
console.log(cachedAdd(4, 5)); 
console.log(cachedAdd(4, 5)); 

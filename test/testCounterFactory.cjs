const counterFactory = require ("../counterFactory.cjs")

try{
    let counter = counterFactory();
    console.log(counter.increment())
    console.log(counter.increment())
    console.log(counter.decrement())
}
catch(error){
    console.log("error")
}
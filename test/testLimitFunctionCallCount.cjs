const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');
try {
    function testCallback() {
        console.log("Callback invoked");
    }
    
    let n = 5;
    const limitCallFunction = limitFunctionCallCount(testCallback, n);
    
    for(let index = 0 ; index <= n+1 ; index++){
        limitCallFunction();
        if(index > n){
            console.log(limitCallFunction());
        }
    }
    
}
catch (error){
    console.log("error")
}
